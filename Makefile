################## SETUP  COMPILER ##################
CC          = sdcc
LD          = sdld
AR          = sdar
AS          = sdasstm8
OBJCOPY     = sdobjcopy
SIZE        = size
MAKE        = make

TARGET      = main
STDLIB      = STM8S_StdPeriph_Lib/Libraries/STM8S_StdPeriph_Driver
INCLUDEDIR  = $(STDLIB)/inc Inc
LIBSRCDIR   = $(STDLIB)/src
LIBSRC      = $(LIBSRCDIR)/stm8s_adc1.c  $(LIBSRCDIR)/stm8s_awu.c
LIBSRC     += $(LIBSRCDIR)/stm8s_beep.c  $(LIBSRCDIR)/stm8s_clk.c
LIBSRC     += $(LIBSRCDIR)/stm8s_exti.c  $(LIBSRCDIR)/stm8s_flash.c
LIBSRC     += $(LIBSRCDIR)/stm8s_gpio.c  $(LIBSRCDIR)/stm8s_i2c.c
LIBSRC     += $(LIBSRCDIR)/stm8s_itc.c   $(LIBSRCDIR)/stm8s_iwdg.c
LIBSRC     += $(LIBSRCDIR)/stm8s_rst.c   $(LIBSRCDIR)/stm8s_spi.c
LIBSRC     += $(LIBSRCDIR)/stm8s_tim1.c  $(LIBSRCDIR)/stm8s_tim2.c
LIBSRC     += $(LIBSRCDIR)/stm8s_uart2.c $(LIBSRCDIR)/stm8s_wwdg.c
LIBSRC     += $(SRCS)/stm8s_it.c         $(SRCS)/uart.c
LIBSRC     += $(SRCS)/tim.c              $(SRCS)/one-wire.c
LIBSRC     += $(SRCS)/DS18x20.c

SRCS        = Src
OBJS        = $(LIBSRC:.c=.rel)

COMPILER    = __SDCC__
DEFINES     = --debug -D$(COMPILER) -DUSE_STDPERIPH_DRIVER -DF_CPU=8000000UL -DUSE_FLOATS=1

CFLAGS      = -mstm8 --std-c99 $(DEFINES)
LDFLAGS     = $(addprefix -I ,$(INCLUDEDIR))

BUILD_DIR   = Build

IHX         = $(BUILD_DIR)/$(TARGET).ihx



################### BUILD PROCESS ###################
.PHONY: all build clean flash

all: build

build: $(OBJS) $(IHX)

$(OBJS):

%.rel: %.c
	mkdir -p $(BUILD_DIR)
	$(CC) -c $(CFLAGS) $(LDFLAGS) -o $(BUILD_DIR)/ $<

$(IHX): $(SRCS)/$(TARGET).c
#	mkdir -p $(BUILD_DIR)
#	$(CC) -c $(CFLAGS) -I$(INCLUDEDIR) -I. -L$(LIBSRCDIR) $(DEFINES) -o $(BUILD_DIR)/ $(LIBSRC)
#	$(CC) -c $(CFLAGS) $(LDFLAGS) $(LIBSRCDIR)/stm8s_beep.c -o $(BUILD_DIR)/
#	$(CC) -c $(CFLAGS) $(LDFLAGS) $(LIBSRCDIR)/stm8s_gpio.c -o $(BUILD_DIR)/
#	$(CC) $(CFLAGS) $(LDFLAGS) -o $(BUILD_DIR)/ $< $(BUILD_DIR)/*.rel
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(BUILD_DIR)/ $< $(BUILD_DIR)/stm8s_clk.rel \
	                                               $(BUILD_DIR)/stm8s_gpio.rel \
	                                               $(BUILD_DIR)/stm8s_uart2.rel \
	                                               $(BUILD_DIR)/stm8s_tim2.rel \
	                                               $(BUILD_DIR)/stm8s_itc.rel \
	                                               $(BUILD_DIR)/stm8s_it.rel \
	                                               $(BUILD_DIR)/uart.rel \
	                                               $(BUILD_DIR)/tim.rel \
	                                               $(BUILD_DIR)/one-wire.rel \
	                                               $(BUILD_DIR)/DS18x20.rel	                                               
	$(SIZE) $@

clean:
	rm -rf $(BUILD_DIR)/*

flash: $(IHX)
	stm8flash -c stlinkv2 -p stm8s105k4 -s flash -w $<

