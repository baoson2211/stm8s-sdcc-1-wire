/**
  * tim.h
  *
  *  Created on: Apr 28, 2017
  *      Author: b4050n
  */

#ifndef __TIM_H
#define __TIM_H

#include "stm8s.h"
#include "stm8s_clk.h"
#include "stm8s_tim2.h"

/* Exported types ------------------------------------------------------------*/
/* Exported defines ----------------------------------------------------------*/
#ifndef F_CPU
#warning F_CPU is not defined!
#endif

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */

void delay_us( uint16_t us );
void delay_ms( uint16_t ms );

#endif /* __ONE_WIRE_H */
